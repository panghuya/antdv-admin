import Mock from 'mockjs2'
import { builder } from '../util'

const menu = (options) => {
  console.log('options', options)
  const menuList = [
    {
      name: 'dashbord',
      icon: 'bxAnaalyse',
      updateTime: '2020-05-20',
      components: 'RouteView',
      path: '/dashboard',
      sort: 0,
      status: 0,
      describe: '这是dashbord的路由',
      children: [
        {
          name: 'dashbord',
          icon: '',
          updateTime: '2020-05-20',
          components: 'dashboard/Analysis',
          path: '/dashboard/analysis',
          sort: 0,
          status: 0,
          describe: '这是dashbord的路由'
        }
      ]
    }
  ]
  return builder(menuList)
}

Mock.mock(/\/api\/menu\/list/, 'get', menu)
