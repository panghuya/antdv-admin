// 部门管理

import request from '@/utils/request'

// 获取菜单列表
export function getRuleList (params) {
  return request({
    url: 'admin/rule',
    method: 'get',
    params: params
  })
}

export function delRule (data) {
  return request({
    url: 'admin/rule',
    method: 'delete',
    data: data
  })
}

export function addRule (data) {
  return request({
    url: 'admin/rule',
    method: 'put',
    data: data
  })
}

export function settingRule (data) {
  return request({
    url: 'admin/rule',
    method: 'post',
    data: data
  })
}
