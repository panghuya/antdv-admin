// 部门管理

import request from '@/utils/request'

// 获取菜单列表
export function getDepartmentList (params) {
  return request({
    url: 'admin/department',
    method: 'get',
    params: params
  })
}

export function delDepartment (data) {
  return request({
    url: 'admin/department',
    method: 'delete',
    data: data
  })
}

export function addDepartment (data) {
  console.log(data)
  return request({
    url: 'admin/department',
    method: 'put',
    data: data
  })
}

export function settingDepartment (data) {
  return request({
    url: 'admin/department',
    method: 'post',
    data: data
  })
}
