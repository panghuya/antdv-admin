// 菜单管理
import request from '@/utils/request'

// 获取菜单列表
export function getMenuList (params) {
  return request({
    url: 'menu/list',
    method: 'get',
    params: params
  })
}

export function delMenu (data) {
  return request({
    url: 'admin/menu',
    method: 'delete',
    data: data
  })
}

export function addMenu (data) {
  return request({
    url: 'admin/menu',
    method: 'put',
    data: data
  })
}

export function settingMenu (data) {
  return request({
    url: 'admin/menu',
    method: 'post',
    data: data
  })
}
