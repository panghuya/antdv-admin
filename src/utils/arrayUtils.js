// 数组操作合集

// 删除数组中的某个元素
export const remove = (arr, ele) => {
  var index = arr.indexOf(ele)
  if (index > -1) {
    arr.splice(index, 1)
  }
  return arr
}
// 数组去重复
export const unique = (arr) => {
  if (Array.hasOwnProperty('from')) {
    return Array.from(new Set(arr))
  } else {
    var n = {}; var r = []
    for (var i = 0; i < arr.length; i++) {
      if (!n[arr[i]]) {
        n[arr[i]] = true
        r.push(arr[i])
      }
    }
    return r
  }
}

// 数组排序 {type} 1：从小到大 2：从大到小 3：随机

export const sort = (arr, type = 1) => {
  return arr.sort((a, b) => {
    switch (type) {
      case 1:
        return a - b
      case 2:
        return b - a
      case 3:
        return Math.random() - 0.5
      default:
        return arr
    }
  })
}
// 判断一个元素是否在数组中

export const contains = (arr, val) => {
  return arr.indexOf(val) !== -1
}
