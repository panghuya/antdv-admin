const _import = require('./_import') // 获取组件的方法
export const asyncRouterHandle = (component) => {
  component = _import(component)
  return component
}
