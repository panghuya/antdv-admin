module.exports = file => {
  return () => import (`@/views/${file}`)
}
