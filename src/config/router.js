// eslint-disable-next-line
import { BlankLayout } from '@/layouts'
import { bxAnaalyse } from '@/core/icons'
// import PageView from '@/layouts/PageView'

export const asyncRouterMap = [
  {
    path: '/',
    component: 'BasicLayout',
    meta: { title: 'menu.home' },
    redirect: '/dashboard/workplace',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: 'RouteView',
        meta: { title: 'menu.dashboard', keepAlive: true, icon: bxAnaalyse },
        children: [
          {
            path: '/dashboard/analysis',
            name: 'Analysis',
            component: 'dashboard/Analysis',
            meta: { title: 'menu.dashboard.analysis', keepAlive: true }
          },
          {
            path: '/dashboard/workplace',
            name: 'Workplace',
            component: 'dashboard/Workplace',
            meta: { title: 'menu.dashboard.workplace', keepAlive: true }
          }
        ]
      },
      // 用户管理
      {
        path: '/users',
        component: 'RouteView',
        meta: { title: '用户管理', keepAlive: true, icon: 'user' },
        children: [
          {
            path: '/users/companyTreeUser',
            name: 'companyTreeUser',
            component: 'profile/advanced/Advanced',
            meta: { title: '内部员工管理', keepAlive: true, icon: 'user' }
          },
          {
            path: '/users/index',
            name: 'usersIndex',
            component: 'users/index',
            meta: { title: '用户管理', keepAlive: true },
            children: [
              {
                path: '/users/agent',
                name: 'usersAgent',
                component: 'users/componets/agent',
                meta: { title: '代理商管理', keepAlive: false }
              },
              {
                path: '/users/company',
                name: 'usersCompany',
                component: 'users/componets/company',
                meta: { title: '企业用户管理', keepAlive: false }
              },
              {
                path: '/users/user',
                name: 'usersUser',
                component: 'users/componets/user',
                meta: { title: '用户管理', keepAlive: false }
              }
            ]
          }
        ]
      },
      {
        path: '/systerm',
        component: 'RouteView',
        meta: { title: '系统设置', keepAlive: true, icon: 'slack' },
        children: [
          {
            path: '/systerm/index',
            name: 'systermAuth',
            component: 'systerm/auth',
            meta: { title: '权限设置模块', keepAlive: true },
            children: [
              {
                path: '/systerm/permissionlist',
                name: 'systermAuthPermission',
                component: 'systerm/authComponents/PermissionList',
                meta: { title: '权限设置', keepAlive: true }
              },
              {
                path: '/systerm/rolelist',
                name: 'systermAuthRoleList',
                component: 'systerm/authComponents/RoleList',
                meta: { title: '角色列表', keepAlive: true }
              },
              {
                path: '/systerm/userlist',
                name: 'systermAuthUserlist',
                component: 'systerm/authComponents/UserList',
                meta: { title: '用户列表', keepAlive: true }
              }
            ]
          },
          {
            path: '/systerm/user',
            name: 'ssystermUser',
            component: 'systerm/user',
            meta: { title: '系统部门', keepAlive: true }
          },
          {
            path: '/systerm/menu',
            name: 'ssystermMenu',
            component: 'systerm/menu',
            meta: { title: '系统菜单管理', keepAlive: true }
          }
        ]
      },
      {
        path: '/other',
        name: 'otherPage',
        component: 'RouteView',
        meta: { title: '其他组件', icon: 'slack', permission: ['dashboard'] },
        redirect: '/other/icon-selector',
        children: [
          {
            path: '/other/icon-selector',
            name: 'TestIconSelect',
            component: 'other/IconSelectorView',
            meta: { title: 'IconSelector', icon: 'tool', keepAlive: true, permission: ['dashboard'] }
          }
        ]
      }
    ]
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/user',
    component: BlankLayout,
    redirect: '/user/login',
    hidden: true,
    children: [
      {
        path: 'login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Login')
      },
      {
        path: 'register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/Register')
      },
      {
        path: 'register-result',
        name: 'registerResult',
        component: () => import(/* webpackChunkName: "user" */ '@/views/user/RegisterResult')
      },
      {
        path: 'recover',
        name: 'recover',
        component: undefined
      }
    ]
  },

  {
    path: '/404',
    component: () => import(/* webpackChunkName: "fail" */ '@/views/exception/404')
  }
]
