> 自定义的model弹出层，用于全局定义弹出层样式
> 


- 主要封装了model弹出层，并且统一了model弹出层的风格 `model.vue`


## API

### props

| 参数               | 说明       | 类型                                 | 是否为必传字段       |
| ---------------- | -------- | ---------------------------------- | --------- |
| visiable         | 控制弹窗变量  | `Boolean`                              | Y |
| title        | 弹窗上方字体 | `String`                             | Y     |
| modelWidth | 弹窗的宽度 | `Number` `String`| 默认为系统储存方案|
## <div style='color: red'>弹窗内容用solt的方式进行传入</div>
```vue
<!--例如-->
<div slot='content'>
内容
</div>
```
### method

| 参数   | 说明     | 类型        | 必传 |
| ---- | ------ | --------- | --- |
| close | 关闭弹窗的回调函数 | `Function` | Y  |
| submit  | 点击确定的回调函数 | `Function`    | Y  |




## 使用方式
```vue
<template>
  <model
  :visiable="modelVisiable"  
  title='名字'
  @close=''
  @submit=''
  >
    <div slot='content'>
      
    </div>
  </model>
</template>

```
```js
import { Model } from '@/components'
export default {
  name: 'Agent',
  components: {
    Model
  },
  data:{
    return {
      modelVisiable : false
    }
  }
}

```