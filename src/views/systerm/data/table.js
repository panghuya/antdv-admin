const data = [
  {
    key: 'list1',
    name: '系统设置',
    icon: 'slack',
    status: 1,
    create_time: '2020-02-01 12:00:12',
    remarks: '这是测试备注',
    children: [
      {
        key: '1',
        name: '系统菜单管理',
        status: 1,
        create_time: '2020-02-01 12:00:12',
        remarks: '这是测试备注'
      },
      {
        key: '2',
        name: '部门管理',
        status: 1,
        create_time: '2020-02-01 12:00:12',
        remarks: '这是测试备注'
      }
    ]
  }
]
const company = [
  {
    key: 1,
    name: '安徽多方通信测试',
    phone: '182999999999',
    payType: 1
  }
]

module.exports = {
  data,
  company
}
