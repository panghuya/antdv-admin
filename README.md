<h1 align='center'>
  自己用的后台模版
</h1>


- 下载下来后 ,进入目录执行
> ` yarn `
>
> 启动 `yarn start`
>
> 编译 `yarn build`


> 账号 ： 182888888888
> 密码 ： 123456

- 注：
> 这个只是我自己使用的后台框架，群里有的人说好看才开源的，没有修改很多东西，需要有一定的基础
> 
> <span style='color:red'>再次强调，需要有一定的基础！！</span>
> 
> 因为本人没有系统学习过，所有文件目录方式都是自己看视频学的，和您使用习惯不一样请自己改，不喜勿喷


- 特定文件目录
 ------
```
 src\components\model   -> model  自己封装的modal层，用于统一风格，相关css请在global.less里面修改
 config\defaultSettings -> config 项目配置文件
 config\router          -> router 动态加载模拟的router文件
```
- 修改了很多ant-design-vue pro 的文件，但大多数文件并没有删除，请根据自己需要自己修改


<h1>modal的使用教程文件</h1>
> 自定义的model弹出层，用于全局定义弹出层样式
> 


- 主要封装了model弹出层，并且统一了model弹出层的风格 `model.vue`


## API

### props

| 参数               | 说明       | 类型                                 | 是否为必传字段       |
| ---------------- | -------- | ---------------------------------- | --------- |
| visiable         | 控制弹窗变量  | `Boolean`                              | Y |
| title        | 弹窗上方字体 | `String`                             | Y     |
| modelWidth | 弹窗的宽度 | `Number` `String`| 默认为系统储存方案|
## <div style='color: red'>弹窗内容用solt的方式进行传入</div>
```vue
<!--例如-->
<div slot='content'>
内容
</div>
```
### method

| 参数   | 说明     | 类型        | 必传 |
| ---- | ------ | --------- | --- |
| close | 关闭弹窗的回调函数 | `Function` | Y  |
| submit  | 点击确定的回调函数 | `Function`    | Y  |




## 使用方式
```vue
<template>
  <model
  :visiable="modelVisiable"  
  title='名字'
  @close=''
  @submit=''
  >
    <div slot='content'>
      
    </div>
  </model>
</template>

```
```js
import { Model } from '@/components'
export default {
  name: 'Agent',
  components: {
    Model
  },
  data:{
    return {
      modelVisiable : false
    }
  }
}

```


